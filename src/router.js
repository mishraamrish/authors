import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/:userType/login",
      name: "login",
      component: () => import("./views/Login.vue")
    },
    {
      path: "/:userType/register",
      name: "register",
      component: () => import("./views/Register.vue")
    },
    {
      path: "/:userType/profile",
      name: "profile",
      component: () => import("./views/Profile.vue")
    },
    {
      path: "/:userType/create-post",
      name: "create-post",
      component: () => import("./views/CreateUpdatePost.vue")
    }
  ]
});
