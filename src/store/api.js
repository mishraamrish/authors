import xhr from "axios";

const axios = xhr.create({ baseURL: "http://127.0.0.1:8000" });
axios.defaults.headers.post["Content-Type"] = "application/json";

const authorApi = xhr.create({ baseURL: "http://127.0.0.1:8000/author" });
authorApi.defaults.headers.post["Content-Type"] = "application/json";

const staffApi = xhr.create({ baseURL: "http://127.0.0.1:8000/staff" });
staffApi.defaults.headers.post["Content-Type"] = "application/json";

const guestAuthorApi = xhr.create({
  baseURL: "http://127.0.0.1:8000/guest-author"
});
guestAuthorApi.defaults.headers.post["Content-Type"] = "application/json";
export { authorApi, guestAuthorApi, staffApi, axios };
