import { guestAuthorApi, authorApi, staffApi, axios } from "../api";
import router from "../../router";

const state = {
  idToken: null,
  userId: null,
  user: null,
  userType: null
};

const mutations = {
  authUser(state, userData) {
    state.idToken = userData.token;
    state.userId = userData.userId;
    state.userType = userData.userType;
  },
  clearAuthData(state) {
    state.idToken = null;
    state.userId = null;
    state.userType = null;
  },
  updateToken(state, token) {
    state.idToken = token;
  }
};

const actions = {
  setLogoutTimer({ commit }, expirationTime) {
    setTimeout(() => {
      commit("clearAuthData");
    }, expirationTime * 1000);
  },
  login({ commit, dispatch, state }, authData) {
    const userType = authData.userType;
    console.log(userType);
    axios
      .post(`/${userType}/login`, {
        email: authData.email,
        password: authData.password
      })
      .then(res => {
        console.log(res);
        const now = new Date();
        const expirationDate = now.getTime() + 259200 * 1000;
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("userId", res.data.user.username);
        localStorage.setItem("expirationDate", expirationDate);
        localStorage.setItem("userType", userType);
        commit("authUser", {
          token: res.data.token,
          userId: res.data.user.username,
          userType: userType
        });
        dispatch("setLogoutTimer", 259200);
        if (userType === "guest-author") {
          guestAuthorApi.defaults.headers.common["Authorization"] = `Bearer ${
            state.idToken
          }`;
        } else if (userType === "staff") {
          staffApi.defaults.headers.common["Authorization"] = `Bearer ${
            state.idToken
          }`;
        } else if (userType === "author") {
          authorApi.defaults.headers.common["Authorization"] = `Bearer ${
            state.idToken
          }`;
        }
      })
      .catch(error => console.log(error));
  },
  tryAutoLogin({ commit, state }) {
    const token = localStorage.getItem("token");
    if (!token) {
      return;
    }
    const expirationDate = localStorage.getItem("expirationDate");
    const now = new Date().getTime();
    if (now >= expirationDate) {
      commit("logOut");
      return;
    }
    const userId = localStorage.getItem("userId");
    const userType = localStorage.getItem("userType");
    if ((expirationDate - now) / 1000 > 86400) {
      commit("authUser", {
        token: token,
        userId: userId,
        userType: userType
      });
    } else {
      axios
        .post("/auth-token", { token: token })
        .then(response => {
          const token = response.data.token;
          const expirationDate = now.getTime() + 259200 * 1000;
          commit("authUser", {
            token: token,
            userId: userId,
            userType: userType
          });
          localStorage.setItem("token", token);
          localStorage.setItem("expirationDate", expirationDate);
        })
        .catch(error => console.log("unable to refresh token", error));
      if (userType === "guest-author") {
        guestAuthorApi.defaults.headers.common["Authorization"] = `Bearer ${
          state.idToken
        }`;
      } else if (userType === "staff") {
        staffApi.defaults.headers.common["Authorization"] = `Bearer ${
          state.idToken
        }`;
      } else if (userType === "author") {
        authorApi.defaults.headers.common["Authorization"] = `Bearer ${
          state.idToken
        }`;
      }
    }
  },
  logOut({ commit }) {
    commit("clearAuthData");
    localStorage.removeItem("expirationDate");
    localStorage.removeItem("token");
    localStorage.removeItem("userId");
    localStorage.removeItem("userType");
    router.replace("/home");
  }
};

const getters = {
  user(state) {
    return {
      username: state.username,
      userType: state.userType
    };
  },
  isAuthenticated(state) {
    if (state.idToken) return state.userType;
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
